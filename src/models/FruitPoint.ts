import { Point } from "./Point";
import { FruitEnum } from "../enums/FruitEnum";

export class FruitPoint extends Point {
  type: FruitEnum = FruitEnum.Banana;

  constructor(props: { x: number; y: number; type?: FruitEnum }) {
    super(props);
    this.x = props.x;
    this.y = props.y;
    if (props.type) {
      this.type = props.type;
    }
  }
}
