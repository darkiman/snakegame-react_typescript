export class Point {
  x: number;
  y: number;

  constructor(props: { x: number; y: number }) {
    this.x = props.x;
    this.y = props.y;
  }

  copy() {
    return new Point({ x: this.x, y: this.y });
  }

  isEqual(point: Point) {
    return point.x === this.x && point.y === this.y;
  }
}
