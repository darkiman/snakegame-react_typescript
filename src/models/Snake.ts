import { Point } from "./Point";
import { MoveEnum } from "../enums/MoveEnum";

export class Snake {
  private _coordinates: Point[] = [];
  private readonly fieldSize: number = 0;
  private lastMove: MoveEnum = MoveEnum.Right;

  constructor(props: {
    initPosition: Point;
    fieldSize: number;
    initialLong?: number;
  }) {
    let { initPosition, fieldSize, initialLong } = props;
    if (!initialLong) {
      initialLong = 3;
    }
    if (initialLong > fieldSize) {
      throw new Error("Snake long more that field");
    }
    for (let i = 0; i < initialLong; i++) {
      const point = new Point({ x: initPosition.x, y: initPosition.y + i });
      this._coordinates.push(point);
    }
    this.fieldSize = fieldSize;
  }

  get coordinates(): Point[] {
    return this._coordinates;
  }

  get isValid(): boolean {
    for (let item of this.coordinates) {
      if (
        item.x >= this.fieldSize ||
        item.y >= this.fieldSize ||
        item.x < 0 ||
        item.y < 0 ||
        this._coordinates.find((elem) => elem.isEqual(item) && elem !== item)
      ) {
        return false;
      }
    }
    return true;
  }

  get header(): Point {
    return this._coordinates[this.coordinates.length - 1];
  }

  private copySnake(snakeHeader: Point): void {
    const snakePart = this.coordinates.slice();
    snakePart.shift();
    snakePart.push(snakeHeader);
    this._coordinates = snakePart;
  }

  private moveRight(): void {
    const snakeHeader = this.header.copy();
    snakeHeader.y = snakeHeader.y + 1;
    this.copySnake(snakeHeader);
  }

  private moveLeft(): void {
    const snakeHeader = this.header.copy();
    snakeHeader.y = snakeHeader.y - 1;
    this.copySnake(snakeHeader);
  }

  private moveUp(): void {
    const snakeHeader = this.header.copy();
    snakeHeader.x = snakeHeader.x - 1;
    this.copySnake(snakeHeader);
  }

  private moveDown(): void {
    const snakeHeader = this.header.copy();
    snakeHeader.x = snakeHeader.x + 1;
    this.copySnake(snakeHeader);
  }

  public move(direction?: MoveEnum): void {
    if (!direction) {
      direction = this.lastMove;
    }
    switch (direction) {
      case MoveEnum.Right:
        if (this.lastMove === MoveEnum.Left) {
          this.moveLeft();
        } else {
          this.moveRight();
          this.lastMove = MoveEnum.Right;
        }
        break;
      case MoveEnum.Left:
        if (this.lastMove === MoveEnum.Right) {
          this.moveRight();
        } else {
          this.moveLeft();
          this.lastMove = MoveEnum.Left;
        }
        break;
      case MoveEnum.Up:
        if (this.lastMove === MoveEnum.Down) {
          this.moveDown();
        } else {
          this.moveUp();
          this.lastMove = MoveEnum.Up;
        }
        break;
      case MoveEnum.Down:
        if (this.lastMove === MoveEnum.Up) {
          this.moveUp();
        } else {
          this.moveDown();
          this.lastMove = MoveEnum.Down;
        }
        break;
    }
  }

  public addToEnd(point: Point): void {
    if (point) {
      this._coordinates.unshift(point);
    }
  }
}
