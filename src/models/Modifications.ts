import { Point } from "./Point";
import { FruitPoint } from "./FruitPoint";

export type Modifications = {
  speed: Point | null;
  long: FruitPoint | null;
};
