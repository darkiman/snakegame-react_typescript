import { Point } from "./Point";

export class GameField {
  field: Point[][] = [];

  constructor(props: { size: number }) {
    const { size } = props;
    const result = [];
    for (let i = 0; i < size; i++) {
      const row = [];
      for (let j = 0; j < size; j++) {
        row.push(new Point({ x: i, y: j }));
      }
      result.push(row);
    }
    this.field = result;
  }
}
