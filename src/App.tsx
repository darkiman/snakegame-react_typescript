import React from "react";
import "./App.css";
import { SnakeGame } from "./components/SnakeGame";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Snake</h2>
        <SnakeGame size={10} initialSpeed={500} initialSnakeLong={3} />
      </header>
    </div>
  );
}

export default App;
