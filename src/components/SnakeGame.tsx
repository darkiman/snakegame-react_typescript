import React, { FC, HTMLProps, useEffect, useState } from "react";
import { Point } from "../models/Point";
import { Snake } from "../models/Snake";
import { MoveEnum } from "../enums/MoveEnum";
import { Modifications } from "../models/Modifications";
import { GameField } from "../models/GameField";
import { FruitPoint } from "../models/FruitPoint";
import { FruitEnum } from "../enums/FruitEnum";

interface FieldProps extends HTMLProps<any> {
  size: number;
  initialSpeed: number;
  initialSnakeLong: number;
}

const DEFAULT_SPEED = 500;
const DEFAULT_FIELD_SIZE = 10;
const DEFAULT_SNAKE_LONG = 3;
// increase speed by 25% on each apple
const SPEED_MODIFICATION = 0.75;
// show speed modificator after 3 long modificator
const LONG_MODIFICATION = 3;

const initField: GameField = new GameField({ size: DEFAULT_FIELD_SIZE });
let initSnake: Snake = new Snake({
  initPosition: new Point({
    x: 0,
    y: 0,
  }),
  fieldSize: DEFAULT_FIELD_SIZE,
  initialLong: DEFAULT_SNAKE_LONG,
});

const initModifications: Modifications = { speed: null, long: null };
let actionStack: MoveEnum[] = [];

const getRandomInt = (min: number, max: number): number => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const refreshPage = () => {
  window.location.reload(false);
};

const getRandomEmptyPoint = (
  field: Point[][],
  snake: Snake,
  modifications: Modifications
): Point => {
  let emptyPoints: Point[] = [];
  for (let row of field) {
    for (let point of row) {
      let isEmpty = true;
      if (
        snake.coordinates.find((item) => item.isEqual(point)) ||
        (modifications.long && modifications.long.isEqual(point)) ||
        (modifications.speed && modifications.speed.isEqual(point))
      ) {
        isEmpty = false;
      }
      if (isEmpty) {
        emptyPoints.push(point);
      }
    }
  }
  const randomIndex = getRandomInt(0, emptyPoints.length - 1);
  return emptyPoints[randomIndex];
};

const getFruitPoint = (
  field: Point[][],
  snake: Snake,
  modifications: Modifications
) => {
  const point = getRandomEmptyPoint(field, snake, modifications);
  const fruitArray = Object.keys(FruitEnum);
  const randomIndex = getRandomInt(0, fruitArray.length - 1);
  const fruitType = fruitArray[randomIndex] as FruitEnum;
  return new FruitPoint({ ...point, type: fruitType });
};

export const SnakeGame: FC<FieldProps> = (props: FieldProps) => {
  const { size, initialSpeed, initialSnakeLong } = props;

  const [gameField, setGameField] = useState(initField);
  const [snake, setSnake] = useState(initSnake);
  const [score, setScore] = useState(0);
  const [speed, setSpeed] = useState(
    initialSpeed ? initialSpeed : DEFAULT_SPEED
  );
  const [speedLevel, setSpeedLevel] = useState(1);
  const [refreshState, setRefreshState] = useState(0);
  const [isGaveOver, setIsGaveOver] = useState(false);
  const [modifications, setModifications] = useState(initModifications);

  useEffect(() => {
    let newGameField;
    let newSnake;
    if (size !== DEFAULT_FIELD_SIZE) {
      newGameField = new GameField({ size });
    }

    if (
      initialSnakeLong !== DEFAULT_SNAKE_LONG ||
      size !== DEFAULT_FIELD_SIZE
    ) {
      newSnake = new Snake({
        initialLong: initialSnakeLong,
        fieldSize: size,
        initPosition: new Point({
          x: 0,
          y: 0,
        }),
      });
    }

    if (newGameField) {
      setGameField(newGameField);
    }
    if (newSnake) {
      setSnake(newSnake);
    }

    setModifications({
      speed: null,
      long: getFruitPoint(
        newGameField ? newGameField.field : gameField.field,
        newSnake ? newSnake : snake,
        modifications
      ),
    });
  }, [size, initialSnakeLong]);

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      switch (e.key) {
        case "ArrowUp":
          actionStack.unshift(MoveEnum.Up);
          break;
        case "ArrowDown":
          actionStack.unshift(MoveEnum.Down);
          break;
        case "ArrowLeft":
          actionStack.unshift(MoveEnum.Left);
          break;
        case "ArrowRight":
          actionStack.unshift(MoveEnum.Right);
          break;
      }
    };
    document.addEventListener("keydown", handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  });

  useEffect(() => {
    setTimeout(() => {
      if (isGaveOver) {
        return;
      }
      const action = actionStack.shift();
      const lastCoordinate = snake.coordinates[0].copy();
      snake.move(action);
      if (
        snake.coordinates.find(
          (item) => modifications.speed && modifications.speed.isEqual(item)
        )
      ) {
        setModifications({
          speed: null,
          long: getFruitPoint(gameField.field, snake, modifications),
        });
        setSpeed(speed * SPEED_MODIFICATION);
        setSpeedLevel(speedLevel + 1);
      } else if (
        snake.coordinates.find(
          (item) => modifications.long && modifications.long.isEqual(item)
        )
      ) {
        snake.addToEnd(lastCoordinate as Point);
        setScore(score + 1);
        const addSpeedModification =
          snake.coordinates.length % LONG_MODIFICATION === 0;
        if (addSpeedModification) {
          setModifications({
            speed: getRandomEmptyPoint(gameField.field, snake, modifications),
            long: null,
          });
        } else {
          setModifications({
            speed: modifications.speed,
            long: getFruitPoint(gameField.field, snake, modifications),
          });
        }
      }
      actionStack = [];
      if (!snake.isValid) {
        setIsGaveOver(true);
        alert("Game over. Refresh page to restart");
        return;
      }
      setRefreshState(refreshState ? 0 : 1);
    }, speed);
  }, [refreshState]);

  const getCellClassName = (point: Point): string => {
    let result = "field-cell ";
    if (snake.coordinates.find((item) => item.isEqual(point))) {
      return (result += "snake-cell");
    }
    if (modifications.speed && modifications.speed.isEqual(point)) {
      return (result += "apple-cell");
    }
    if (modifications.long && modifications.long.isEqual(point)) {
      return (result += `${modifications.long.type.toLocaleLowerCase()}-cell`);
    }
    return result;
  };

  return (
    <div className={"field-container"}>
      <div
        className={"info-container"}
        style={{ flexDirection: isGaveOver ? "column" : "row" }}
      >
        <span>Score: {score}</span>
        <span>Speed level: {speedLevel}</span>
      </div>
      {isGaveOver ? (
        <button onClick={refreshPage}>Refresh page</button>
      ) : (
        <div className={"field-container"}>
          {gameField.field.map((row, rowIndex) => {
            return (
              <div key={rowIndex} className={"field-row"}>
                {row.map((point) => {
                  return (
                    <div
                      key={`${point.x}-${point.y}`}
                      className={getCellClassName(point)}
                    />
                  );
                })}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};
