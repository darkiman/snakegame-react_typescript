export enum FruitEnum {
    Grape = "grape",
    Banana = "banana",
    Strawberry = "strawberry",
}
