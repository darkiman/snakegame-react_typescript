export enum MoveEnum {
  Right = "right",
  Left = "left",
  Up = "up",
  Down = "down",
}
